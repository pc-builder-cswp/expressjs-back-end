const express = require('express');

const router = express.Router();
const jwt = require('../helpers/jwt');

const buildRoute = require('./apiv1Routes/buildRoute');
const commentRoute = require('./apiv1Routes/commentRoute');
const partRoute = require('./apiv1Routes/partRoute');
const userRoute = require('./apiv1Routes/userRoute');

router.all('*', (req, res, next) => {
  const token = req.header('token') || '';

  jwt.decodeToken(token, (err, payload) => {
    if (err) {
      res
        .status(401)
        .json({
          message: 'Not authorized / authenticated!',
          error: err,
        });
    } else {
      res.set('id', payload.sub);
      next();
    }
  });
});

router.use('/builds', buildRoute);
router.use('/comments', commentRoute);
router.use('/parts', partRoute);
router.use('/users', userRoute);

module.exports = router;
