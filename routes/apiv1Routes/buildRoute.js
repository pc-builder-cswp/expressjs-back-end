/*
 * @Author: b.kho
 * @Date: 2019-11-22 01:01:02
 * @Last Modified by: b.kho
 * @Last Modified time: 2020-04-01 17:44:34
 */

const express = require('express');

const router = express.Router();

const assert = require('assert');

// Import ObjectId
const { ObjectId } = require('mongoose').Types;

const Build = require('../../models/build');
const User = require('../../models/user');
const Comment = require('../../models/comment');

// POST: create new build
router.post('/', (req, res) => {
  const b = req.body;
  const userId = res.get('id');

  const tle = b.title;
  const des = b.description;
  const { cpu } = b;
  const { cpuCooler } = b;
  const { motherboard } = b;
  const { memory } = b;
  const { storage } = b;
  const { gpu } = b;
  const { pcCase } = b;
  const { psu } = b;

  try {
    assert(typeof tle === 'string', 'title is not a string!');
    assert(typeof des === 'string', 'description is not a string!');
    assert(typeof cpu === 'string', 'cpu is not a string!');
    assert(typeof cpuCooler === 'string', 'cpuCooler is not a string!');
    assert(typeof motherboard === 'string', 'motherboard is not a string!');
    assert(typeof memory === 'string', 'memory is not a string!');
    assert(typeof storage === 'string', 'storage is not a string!');
    assert(typeof gpu === 'string', 'gpu is not a string!');
    assert(typeof pcCase === 'string', 'pcCase is not a string!');
    assert(typeof psu === 'string', 'psu is not a string!');

    Build.create({
      user: ObjectId(userId),
      title: tle,
      description: des,
      cpu: ObjectId(cpu),
      cpuCooler: ObjectId(cpuCooler),
      motherboard: ObjectId(motherboard),
      memory: ObjectId(memory),
      storage: ObjectId(storage),
      gpu: ObjectId(gpu),
      pcCase: ObjectId(pcCase),
      psu: ObjectId(psu),
    })
      .then((build) => {
        User.updateOne({ _id: userId }, { $push: { builds: build._id } })
          .then(() => {
            Build.findOne({ _id: build._id })
              .populate('user')
              .populate('cpu')
              .populate('cpuCooler')
              .populate('motherboard')
              .populate('memory')
              .populate('storage')
              .populate('gpu')
              .populate('pcCase')
              .populate('psu')
              .then((finalBuild) => {
                res.status(200).json({
                  message: 'Build succesfully created!',
                  build: finalBuild,
                });
              })
              .catch((error) => {
                console.log(error);
                res.status(500).json({
                  message: 'Could not create / find build!',
                  error,
                });
              });
          })
          .catch((error) => {
            console.log(error);
            res.status(500).json({
              message: 'Could not create build!',
              error: error.toString(),
            });
          });
      })
      .catch((error) => {
        console.log(error);
        res.status(500).json({
          message: 'Could not create build!',
          error: error.toString(),
        });
      });
  } catch (ex) {
    console.log(ex);
    res.status(500).json({
      message: 'An exception has occurred!',
      exception: ex.toString(),
    });
  }
});

// GET: all builds
router.get('/', (req, res) => {
  Build.find({})
    .populate('user')
    .populate('cpu')
    .populate('cpuCooler')
    .populate('motherboard')
    .populate('memory')
    .populate('storage')
    .populate('gpu')
    .populate('pcCase')
    .populate('psu')
    .then((result) => {
      res.status(200).json(result);
    })
    .catch((error) => {
      res
        .status(500)
        .json({ message: 'Could not get builds!', error: error.toString() });
    });
});

// GET: build with id
router.get('/:id', (req, res) => {
  const { id } = req.params;

  Build.findOne({ _id: id })
    .populate('user')
    .populate('cpu')
    .populate('cpuCooler')
    .populate('motherboard')
    .populate('memory')
    .populate('storage')
    .populate('gpu')
    .populate('pcCase')
    .populate('psu')
    .then((build) => {
      res.status(200).json(build);
    })
    .catch((error) => {
      res.status(500).json({ message: 'Could not find build!', error });
    });
});

// GET: comments of build with id
router.get('/:id/comments', (req, res) => {
  const { id } = req.params;

  Comment.find({ build: id })
    .populate('user')
    .select('-password')
    .select('-emailaddress')
    .then((comments) => {
      res.status(200).json(comments);
    })
    .catch((error) => {
      res
        .status(500)
        .json({ message: 'Could not get comments of the part!', error });
    });
});

// PUT: update build
router.put('/:id', (req, res) => {
  const b = req.body;
  const buildId = req.params.id;

  const { title } = b;
  const { description } = b;
  const { cpu } = b;
  const { cpuCooler } = b;
  const { motherboard } = b;
  const { memory } = b;
  const { storage } = b;
  const { gpu } = b;
  const { pcCase } = b;
  const { psu } = b;

  try {
    assert(typeof title === 'string', 'title is not a string!');
    assert(typeof description === 'string', 'description is not a string!');
    assert(typeof cpu === 'string', 'cpu is not a string!');
    assert(typeof cpuCooler === 'string', 'cpuCooler is not a string!');
    assert(typeof motherboard === 'string', 'motherboard is not a string!');
    assert(typeof memory === 'string', 'memory is not a string!');
    assert(typeof storage === 'string', 'storage is not a string!');
    assert(typeof gpu === 'string', 'gpu is not a string!');
    assert(typeof pcCase === 'string', 'pcCase is not a string!');
    assert(typeof psu === 'string', 'psu is not a string!');

    Build.updateOne(
      {
        _id: buildId,
      },
      {
        $set: {
          title,
          description,
          cpu: ObjectId(cpu),
          cpuCooler: ObjectId(cpuCooler),
          motherboard: ObjectId(motherboard),
          memory: ObjectId(memory),
          storage: ObjectId(storage),
          gpu: ObjectId(gpu),
          pcCase: ObjectId(pcCase),
          psu: ObjectId(psu),
        },
      },
    )
      .then((build) => {
        User.updateOne({ _id: res.get('id') }, { $push: { builds: build._id } })
          .then(() => {
            res
              .status(200)
              .json({ message: 'Build succesfully updated!', build });
          })
          .catch((error) => {
            res
              .status(500)
              .json({ message: 'Could not update build!', error });
          });
      })
      .catch((error) => {
        res
          .status(500)
          .json({ message: 'Could not update build!', error });
      });
  } catch (ex) {
    res.status(500).json({
      message: 'An exception has occurred!',
      exception: ex.toString(),
    });
  }
});

// PUT: like/unlike a builds
router.put('/:id/like', (req, res) => {
  const buildId = req.params.id;
  const userId = res.get('id');

  Build.findOne({ _id: buildId })
    .then((build) => {
      //
      // Check if like of user exists
      //
      if (build.upvotes.filter((e) => e._id === userId) > 0) {
        //
        // Unlike
        //
        Build.updateOne(
          { _id: buildId },
          { $pull: { upvotes: ObjectId(userId) } },
        )
          .then((result) => {
            res
              .status(200)
              .json({ message: 'Removed upvote to build!', build: result });
          })
          .catch((err) => {
            res
              .status(500)
              .json({ message: 'Could not add upvote build', error: err });
          });
      } else {
        //
        // Like
        //
        Build.updateOne(
          { _id: buildId },
          { $push: { upvotes: ObjectId(userId) } },
        )
          .then((result) => {
            res
              .status(200)
              .json({ message: 'Added upvote to build!', build: result });
          })
          .catch((err) => {
            res
              .status(500)
              .json({ message: 'Could not add upvote build', error: err });
          });
      }
    })
    .catch((err) => {
      res.status(500).json({ message: 'Could not find build', error: err });
    });
});

// DELETE: delete build
router.delete('/:id', (req, res) => {
  const buildId = req.params.id;
  const userId = res.get('id');

  Build.findOneAndDelete({ _id: buildId, user: userId })
    .then(() => {
      res
        .status(200)
        .json({ message: 'Build succesfully deleted!', build: buildId });
    })
    .catch((err) => {
      res
        .status(500)
        .json({ message: 'Could not delete build!', error: err });
    });
});

module.exports = router;
