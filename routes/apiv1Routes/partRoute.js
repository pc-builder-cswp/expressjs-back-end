const express = require('express');

const router = express.Router();

const assert = require('assert');

const Part = require('../../models/part');
const User = require('../../models/user');
const Comment = require('../../models/comment');

// POST: new part
router.post('/', (req, res) => {
  const b = req.body;

  const { name } = b;
  const { description } = b;
  const { price } = b;
  const { ram } = b;
  const { size } = b;
  const { manufacturer } = b;
  const { partType } = b;
  const { powerUsage } = b;
  const { releaseDate } = b;
  const { imgUrl } = b;

  assert(typeof name === 'string', 'name is not a string!');
  assert(typeof description === 'string', 'description is not a string!');
  assert(typeof price === 'number', 'price is not a valid number!');
  assert(typeof ram === 'number', 'ram is not a valid number!');
  assert(typeof size === 'string', 'size is not a valid string');
  assert(
    typeof manufacturer === 'string',
    'manufacturer is not a valid string!',
  );
  assert(typeof partType === 'string', 'partType is not a valid string!');
  assert(typeof powerUsage === 'number', 'powerUsage is not a valid number!');
  assert(typeof releaseDate === 'string', 'releaseDate is not a valid string date!');
  assert(typeof imgUrl === 'string', 'imgUrl is not a valid string!');

  Part.create({
    name,
    description,
    price,
    ram,
    size,
    manufacturer,
    partType,
    powerUsage,
    releaseDate,
    imgUrl,
  })
    .then(() => {
      res.status(200).json({ message: 'Part has been succesfully created.' });
    })
    .catch((error) => {
      console.log(error);
      res.status(500).json({ message: 'Could not create part!', error: error.toString() });
    });
});

// POST: add part to user's favouriteParts
router.post('/:id', (req, res) => {
  const userId = res.get('id');
  const partId = req.params.id;

  Part.findOneAndUpdate({ _id: partId }, { $push: { favoriteOfUsers: userId } })
    .then(() => {
      User.findOneAndUpdate(
        { _id: userId },
        { $push: { favouriteParts: partId } },
      )
        .then(() => {
          res
            .status(200)
            .json({ message: 'Part succesfully added to favourite of user!' });
        })
        .catch((err) => {
          res.status(500).json({
            message: 'Could not add part to favouriteParts of user!',
            error: err,
          });
        });
    })
    .catch((err) => {
      res.status(500).json({
        message: 'Could not add user to favouriteOfUsers of part!',
        error: err,
      });
    });
});

// GET: all parts
router.get('/', (req, res) => {
  Part.find({})
    .then((result) => {
      res.status(200).json(result);
    })
    .catch((err) => {
      res.status(500).json({ message: 'Could not find parts!', error: err });
    });
});

// GET: single part with id
router.get('/:id', (req, res) => {
  const { id } = req.params;

  Part.findOne({ _id: id })
    .then((part) => {
      res.status(200).json(part);
    })
    .catch((err) => {
      res
        .status(500)
        .json({ message: 'Could not find the part!', error: err });
    });
});

// GET: comments of part with id
router.get('/:id/comments', (req, res) => {
  const { id } = req.params;

  Comment.find({ part: id })
    .populate('user')
    .select('username')
    .select('_id')
    .select('content')
    .select('postDate')
    .then((comments) => {
      res.status(200).json(comments);
    })
    .catch((error) => {
      res
        .status(500)
        .json({ message: 'Could not get comments of the part!', error });
    });
});

module.exports = router;
