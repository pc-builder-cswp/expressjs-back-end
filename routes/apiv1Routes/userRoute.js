const express = require('express');
const assert = require('assert');

const router = express.Router();

const bcrypt = require('bcryptjs');

const User = require('../../models/user');
const Comment = require('../../models/comment');
const Build = require('../../models/build');

// Saltrounds for password
const saltRounds = 10;

// GET: info of logged in user
router.get('/', (req, res) => {
  User.findOne({ _id: res.get('id') }, { password: 0 })
    .then((user) => {
      if (user === null) {
        res.status(204).json({ message: 'No user found!' });
      } else {
        res.status(200).json({ user });
      }
    })
    .catch((err) => {
      res.status(500).json({
        message: 'Could not find user!',
        error: err,
      });
    });
});

// GET: builds of logged in user
router.get('/builds', (req, res) => {
  Build.find({ user: res.get('id') })
    .then((builds) => {
      res.status(200).json({ builds });
    })
    .catch((err) => {
      res.status(500).json({
        message: 'Could not find builds!',
        error: err,
      });
    });
});

// GET: comments of logged in user
router.get('/comments', (req, res) => {
  Comment.find({ user: res.get('id') }, { comments: 1 })
    .populate('comments')
    .then((comments) => {
      res.status(200).json({ comments });
    })
    .catch((err) => {
      res.status(500).json({
        message: 'Could not find comments!',
        error: err,
      });
    });
});

// PUT: UPDATE PASSWORD
router.put('/resetpassword', (req, res) => {
  try {
    const b = req.body;

    const userId = res.get('id');
    const { password } = b;
    const { newPassword } = b;

    assert(typeof password === 'string', 'password is not a string!');
    assert(typeof newPassword === 'string', 'newPassword is not a string!');

    User.findOne({ _id: userId }).then((user) => {
      if (bcrypt.compareSync(password, user.password)) {
        User.findByIdAndUpdate(userId, {
          password: bcrypt.hashSync(newPassword, saltRounds),
        })
          .then(() => {
            res.status(200).json(`User ${user._id} password updated!`);
          })
          .catch((err) => {
            console.log(err);
            res.status(500).json({
              message: 'Could not update password of user',
              error: err,
            });
          });
      } else {
        res.status(401).json({
          message: 'Password update failed, because password does not match with the current password',
        });
      }
    })
      .catch((err) => {
        res.status(500).json({
          message: 'Could not find user!',
          error: err,
        });
      });
  } catch (ex) {
    res.status(500).json({
      message: 'An exception has occurred!',
      exception: ex.toString(),
    });
  }
});

// DELETE: account of logged in user
router.delete('/', (req, res) => {
  const id = res.get('id');

  const { password } = req.body;

  User.findOne({ _id: id })
    .then((user) => {
      if (bcrypt.compareSync(password, user.password)) {
        User.findOneAndDelete({ _id: id })
          .then(() => {
            res
              .status(200)
              .json({ message: `User ${user._id} succesfully deleted!` });
          })
          .catch((err) => {
            res.status(500).json({
              message: 'Unable to find user',
              error: err,
            });
          });
      } else {
        res.status(403).json({ message: 'Not authorized to do this action!' });
      }
    })
    .catch((err) => {
      res.status(500).json({
        message: 'Could not find user!',
        error: err,
      });
    });
});

module.exports = router;
