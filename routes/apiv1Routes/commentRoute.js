const express = require('express');

const router = express.Router();

const assert = require('assert');

const { ObjectId } = require('mongoose').Types;

const Comment = require('../../models/comment');
const User = require('../../models/user');
const Part = require('../../models/part');
const Build = require('../../models/build');

// POST: new comment for build or part
router.post('/', (req, res) => {
  try {
    const b = req.body;

    const userId = res.get('id');
    const con = b.content;

    assert(typeof con === 'string', 'content is not a string!');

    let commentObj = {};

    if (!b.buildId) {
      commentObj = {
        user: ObjectId(userId),
        part: b.partId,
        content: con,
      };
    } else {
      commentObj = {
        user: ObjectId(userId),
        build: b.buildId,
        content: con,
      };
    }

    Comment.create(commentObj)
      .then((comment) => {
        if (!b.buildId) {
          Part.updateOne(
            { _id: b.partId },
            { $push: { comments: ObjectId(comment._id) } },
          )
            .then(() => {
              Comment.findOne({ _id: comment._id })
                .populate('user')
                .then((finalComment) => {
                  res.status(200).json({
                    message: 'Comment succesfully created',
                    comment: finalComment,
                  });
                });
            })
            .catch((err) => {
              console.log(err);
              res.status(500).json({
                message: 'Unable to create comment and update Part!',
                error: err.toString(),
              });
            });
        } else {
          Build.updateOne(
            { _id: b.buildId },
            { $push: { comments: ObjectId(comment._id) } },
          )
            .then(() => {
              Comment.findOne({ _id: comment._id }).then((finalComment) => {
                res.status(200).json({
                  message: 'Comment succesfully created',
                  comment: finalComment.populate('user'),
                });
              });
            })
            .catch((err) => {
              console.log(err);
              res.status(500).json({
                message: 'Unable to create comment and update build!',
                error: err.toString(),
              });
            });
        }
      })
      .catch((err) => {
        console.log(err);
        res.status(500).json({
          message: 'Unable to create comment!',
          error: err.toString(),
        });
      });
  } catch (ex) {
    console.log(ex);
    res.status(500).json({
      message: 'An error has occurred!',
      exception: ex.message,
    });
  }
});

// GET: all comments
router.get('/', (req, res) => {
  Comment.find({})
    .select('username')
    .populate('user', 'username')
    .then((result) => {
      res.status(200).json(result);
    })
    .catch((err) => {
      res.status(500).json({
        message: 'Could not get comments!',
        error: err.toString(),
      });
    });
});

// GET: comment with id
router.get('/:id', (req, res) => {
  const { id } = req.params;

  Comment.findOne({ _id: id })
    .select('username')
    .populate('user', 'username')
    .then((comment) => {
      res.status(200).json(comment);
    })
    .catch((err) => {
      res.status(500).json({
        message: 'Could not get comment!',
        error: err.toString(),
      });
    });
});

// PUT: update a comment
router.put('/:id', (req, res) => {
  try {
    const { id } = req.params;

    const con = req.body.content;

    assert(typeof con === 'string', 'Content is not a string!');

    Comment.findOneAndUpdate({ _id: id }, { $set: { content: con } })
      .select('username')
      .populate('user', 'username')
      .then((comment) => {
        res.status(200).json({
          message: 'Comment succesfully updated!',
          comment,
        });
      })
      .catch((err) => {
        res.status(500).json({
          message: 'Unable to update comment!',
          error: err.toString,
        });
      });
  } catch (ex) {
    res.status(500).json({
      message: 'An exception has occurred!',
      exception: ex.message,
    });
  }
});

// DELETE: delete a comment
router.delete('/:id', (req, res) => {
  const commentId = req.params.id;
  const userId = res.get('id');

  Comment.deleteOne({ _id: commentId, user: userId })
    .then(() => {
      User.updateOne({ _id: userId }, { $pull: { comments: commentId } })
        .then(() => {
          res.status(200).json({ message: 'Comment succesfully deleted!' });
        })
        .catch((err) => {
          res.status(500).json({
            message: 'Unable to delete comment from user!',
            error: err.toString(),
          });
        });
    })
    .catch((err) => {
      res.status(500).json({
        message: 'Unable to delete comment from user!',
        error: err.toString(),
      });
    });
});

module.exports = router;
