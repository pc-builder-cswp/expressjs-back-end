const express = require('express');
const assert = require('assert');

const logger = require('tracer').dailyfile({
  root: './logs',
  maxLogFiles: 10,
  allLogsFileName: 'pc-builder-app',
  format: '{{timestamp}} <{{title}}> {{message}} (in {{file}}:{{line}})',
  dateformat: 'HH:MM:ss.L',
});

const router = express.Router();
const bcrypt = require('bcryptjs');
const User = require('../models/user');
const jwt = require('../helpers/jwt');

// Saltrounds for bcrypt
const saltRounds = 10;

// REGISTER NEW USER
router.post('/register', (req, res) => {
  try {
    const b = req.body;

    const usrName = b.username;
    const { password } = b;
    const emailAddress = b.emailaddress;

    assert(typeof usrName === 'string', 'Invalid Username!');
    assert(typeof password === 'string', 'Invalid Password!');
    assert(typeof emailAddress === 'string', 'Invalid emailaddress');

    const hash = bcrypt.hashSync(password, saltRounds);

    const newUser = new User({
      username: usrName,
      password: hash,
      emailaddress: emailAddress,
    });

    newUser
      .save()
      .then((user) => {
        logger.log(`New user registered: ${user.username}`);
        const token = jwt.encodeToken(user._id);
        res.status(200).json({
          message: 'Registration successful!',
          user,
          token,
        });
        console.log(`new user created: ${usrName}`);
      })
      .catch((err) => {
        logger.error(err);
        console.error(err);

        res.status(500).json({
          message: 'Registration failed: username already exists',
          error: err,
        });
      });
  } catch (ex) {
    res
      .status(500)
      .json({ message: 'Registration failed!', exception: ex.toString() });
    console.log(ex);
  }
});

// LOGIN
router.post('/login', (req, res) => {
  const b = req.body;

  try {
    assert(typeof b.username === 'string', 'userName is not a string!');

    User.findOne({ username: b.username })
      .then((user) => {
        if (bcrypt.compareSync(b.password, user.password)) {
          logger.log(`Logged in: user '${user.username}'`);

          const token = jwt.encodeToken(user._id);

          res.status(200).json({
            message: 'Succesful login!',
            user,
            token,
          });
        } else {
          logger.log(`Attempted login failed: ${b}`);

          res.status(401).json({ message: 'Authentication failed!' });
        }
      })
      .catch((error) => {
        res.status(401).json({
          message: 'Could not authenticate user!',
          error: error.toString(),
        });
      });
  } catch (ex) {
    res.status(500).json({ error: ex.toString() });
  }
});

module.exports = router;
