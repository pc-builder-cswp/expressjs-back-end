const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

const logger = require('tracer').dailyfile({
  root: './logs',
  maxLogFiles: 10,
  allLogsFileName: 'pc-builder-app',
  format: '{{timestamp}} <{{title}}> {{message}} (in {{file}}:{{line}})',
  dateformat: 'HH:MM:ss.L',
});

const config = require('./config/config.json');
const apiv1 = require('./routes/apiv1');
const auth = require('./routes/auth');

// Initialize express.js app
const app = express();
app.use(cors({ origin: true, credentials: true }));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Index page
app.get('', (req, res) => {
  res.status(200).json({ response: 'Welcome to the pc-builder-api V1.1.0' });
});

// CORS
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});


// Middleware: logging for all requests
app.all('*', (req, res, next) => {
  logger.info('%s', req.hostname, req.url);
  console.log(`${req.hostname} | ${req.url}`);
  next();
});

// ROUTES
app.use('/apiv1', apiv1);
app.use('/auth', auth);

// CONNECT TO DB
mongoose.connect(
  config.db.connectionString,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
  },
  (err) => {
    if (err) {
      console.log(err);
      logger.error(err);
    } else {
      console.log('Connected to DB!');
      logger.log('Connected to DB!');
    }
  },
);

// ERROR HANDLER
function errorHandler(err, req, res) {
  console.log(err);
  res.status(500).json(err);
}

app.use(errorHandler);

// RUN SERVER
const port = process.env.PORT || 6060;

const server = app.listen(port, (err) => {
  if (err) {
    console.error(err);
    logger.error(err);
  } else {
    console.log(`Server is running on port ${server.address().port}`);
    logger.log(`Server running on port ${server.address().port}`);
  }
});

module.exports = app;
