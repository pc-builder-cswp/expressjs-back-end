const moment = require('moment');
const jwt = require('jwt-simple');
const connection = require('../config/config');

//
// Encode (from userId to token)
//
function encodeToken(id) {
  const payload = {
    exp: moment()
      .add(1, 'days')
      .unix(),
    iat: moment().unix(),
    sub: id,
  };
  return jwt.encode(payload, connection.db.secretKey);
}

//
// Decode (from token to username)
//
function decodeToken(token, cb) {
  try {
    const payload = jwt.decode(token, connection.db.secretKey);

    // Check if the token has expired
    if (moment().unix() > payload.exp) {
      cb(new Error('token_has_expired'));
    } else {
      cb(null, payload);
    }
  } catch (err) {
    cb(err, null);
  }
}

module.exports = {
  encodeToken,
  decodeToken,
};
