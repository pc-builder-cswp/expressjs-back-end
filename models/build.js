const mongoose = require('mongoose');

const { Schema } = mongoose;
const BuildSchema = new Schema(
  {
    user: {
      type: Schema.Types.ObjectId,
      ref: 'user',
      required: [true, 'user is required'],
    },
    createDate: {
      type: Date,
      default: Date.now(),
    },
    title: {
      type: String,
      unique: true,
      required: [true, 'Title is required'],
    },
    description: {
      type: String,
      required: [true, 'Description is required'],
    },
    cpu: {
      type: Schema.Types.ObjectId,
      ref: 'part',
      required: [true, 'Cpu is required'],
    },
    cpuCooler: {
      type: Schema.Types.ObjectId,
      ref: 'part',
      required: [true, 'CpuCooler is required'],
    },
    motherboard: {
      type: Schema.Types.ObjectId,
      ref: 'part',
      required: [true, 'motherboard is required'],
    },
    memory: {
      type: Schema.Types.ObjectId,
      ref: 'part',
      required: [true, 'memory is required'],
    },
    storage: {
      type: Schema.Types.ObjectId,
      ref: 'part',
      required: [true, 'storage is required'],
    },
    gpu: {
      type: Schema.Types.ObjectId,
      ref: 'part',
      required: [true, 'gpu is required'],
    },
    pcCase: {
      type: Schema.Types.ObjectId,
      ref: 'part',
      required: [true, 'pcCase is required'],
    },
    psu: {
      type: Schema.Types.ObjectId,
      ref: 'part',
      required: [true, 'psu is required'],
    },
    comments: [{ type: Schema.Types.ObjectId, ref: 'comment' }],
    upvotes: [{ type: Schema.Types.ObjectId, ref: 'user' }],
  },
  {
    collection: 'builds',
    toObject: { virtuals: true },
    toJSON: { virtuals: true },
  },
);

// Virtual: get total count of comments
BuildSchema.virtual('commentsCount').get(function () {
  return this.comments.length;
});

// Virtual: get total count of upvotes
BuildSchema.virtual('upvotesCount').get(function () {
  return this.upvotes.length;
});

const Build = mongoose.model('build', BuildSchema);

module.exports = Build;
