const mongoose = require('mongoose');

const { Schema } = mongoose;

// Email address Regex from: https://emailregex.com/
// Based on the General Email Regex (RFC 5322 Official Standard) Regex

const UserSchema = new Schema(
  {
    username: {
      type: String,
      validate: {
        validator(v) {
          return /^[a-zA-Z0-9_-]{3,16}$/.test(v);
        },
        msg:
          'username is not valid (can only contain lower- and uppercase letters and numbers; Min. characters is 3, max. 16)',
      },
      unique: [true, 'username is already in use!'],
      required: [
        true,
        'Name is required (can only contain lower- and uppercase letters and numbers; Min. characters is 3, max. 16)',
      ],
    },
    password: {
      type: String,
      required: [true, 'password is required'],
    },
    emailaddress: {
      type: String,
      validate: {
        validator(v) {
          return /^(([^<>()\\[\]\\.,;:\s@"]+(\.[^<>()\\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
            v,
          );
        },
        msg: 'emailAddress is not valid!',
      },
    },
    registerDate: {
      type: Date,
      default: Date.now(),
    },
    builds: [{ type: Schema.Types.ObjectId, ref: 'build' }],
    comments: [{ type: Schema.Types.ObjectId, ref: 'comment' }],
  },
  {
    collection: 'users',
    toObject: { virtuals: true },
    toJSON: { virtuals: true },
  },
);

// Virtual value for build count
UserSchema.virtual('buildCount').get(function () {
  return this.builds.length;
});

// Virtual value for comment count
UserSchema.virtual('commentCount').get(function () {
  return this.comments.length;
});

// Delete constraint of user for builds
UserSchema.pre('remove', function (next) {
  const Build = mongoose.model('build');

  Build.deleteMany({ _id: { $in: this.builds } }).then(() => next());
});

// Delete constraint of user for comments
UserSchema.pre('remove', function (next) {
  const Comment = mongoose.model('comment');

  Comment.deleteMany({ _id: { $in: this.comments } }).then(() => next());
});

const User = mongoose.model('user', UserSchema);

module.exports = User;
