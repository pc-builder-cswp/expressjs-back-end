const mongoose = require('mongoose');

const { Schema } = mongoose;

const CommentSchema = new Schema(
  {
    user: {
      type: Schema.Types.ObjectId,
      ref: 'user',
      required: [true, 'User is required'],
    },
    build: {
      type: Schema.Types.ObjectId,
      ref: 'build',
    },
    part: {
      type: Schema.Types.ObjectId,
      ref: 'part',
    },
    postDate: {
      type: Date,
      default: Date.now(),
    },
    content: {
      type: String,
      required: [true, 'Comment is required'],
    },
  },
  { collection: 'comments' },
);

const Comment = mongoose.model('comment', CommentSchema);

module.exports = Comment;
