const mongoose = require('mongoose');

const { Schema } = mongoose;

const PartSchema = new Schema(
  {
    name: {
      type: String,
      unique: true,
      required: [true, 'Name is required'],
    },
    description: {
      type: String,
      required: [true, 'Description is required.'],
    },
    price: {
      type: Number,
      required: [true, 'Price is required'],
    },
    ram: {
      type: Number,
      default: 0,
    },
    size: {
      type: String,
      validate: {
        validator(v) {
          return /^(ATX|mATX|ITX|)$/.test(v);
        },
        msg: 'Size can only be ATX, mATX or ITX !',
      },
    },
    manufacturer: {
      type: String,
      required: [true, 'manufacturerId is required'],
    },
    partType: {
      type: String,
      required: [true, 'partType is required'],
    },
    powerUsage: {
      type: Number,
      required: [true, 'Power Usage is required in Watt'],
    },
    releaseDate: {
      type: Date,
      required: [true, 'releaseDate is required!'],
    },
    imgUrl: {
      type: String,
    },
  },
  {
    collection: 'parts',
    toObject: { virtuals: true },
    toJSON: { virtuals: true },
  },
);

const Part = mongoose.model('part', PartSchema);

module.exports = Part;
